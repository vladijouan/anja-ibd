`mkdir TRIMMED`

`mkdir CLEAN_READS`

`mkdir MAPPING`

`mkdir BAMS`

`mkdir DEDUPBAMS`

`mkdir FREEBAYES`

`mkdir TOTALPOP`

`mkdir PHASING`

`mkdir IBD`

#This is the trimming procedure:

`for FILE in *R1_001.fastq.gz; do NAME=$(basename $FILE _R1_001.fastq.gz); cutadapt -a file:adapters1.fa -A file:adapters2.fa -j5 -m20 -q20,20 -o ../TRIMMED/$NAME\_R1_001.trimmed.fastq.gz -p ../TRIMMED/$NAME\_R2_001.trimmed.fastq.gz $NAME\_R1_001.fastq.gz $NAME\_R2_001.fastq.gz; done &>> ../TRIMMED/outputlog.txt`

#Concatenate the trimmed reads from the same sample into one file:

`cd ../TRIMMED/`

`ls *.fastq.gz | cut -d"_" -f2 | sort -u > list_of_samples.txt`

`while IFS= read -r LINE; do echo $LINE; cat *$LINE*R1*.gz > ../CLEAN_READS/$LINE.R1.fastq.gz; cat *$LINE*R2*.gz > ../CLEAN_READS/$LINE.R2.fastq.gz; done < list_of_samples.txt`

#Mapping:

`cd ../CLEAN_READS/`

`for FILE in *R1.fastq.gz*; do NAME=$(basename $FILE .R1.fastq.gz); echo $NAME; hisat2 -p 25 -x /buffer/ag_bsc/AnjasMacaques/genome/Macgenome -1 $NAME.R1.fastq.gz -2 $NAME.R2.fastq.gz -S ../MAPPING/$NAME.sam &>>../MAPPING/$NAME.mapping.log; done`

#preparing the BAM files (sorting and indexing)

`cd ../MAPPING/`

`for FILE in *.sam; do NAME=$(basename $FILE .sam); echo $NAME; samtools sort -o ../BAMS/$NAME.bam -@ 20 $FILE; done`

`cd ../BAMS/`

`for FILE in *.bam; do samtools index $FILE; done`

`for FILE in *.bam; do NAME=$(basename $FILE .bam); echo $NAME; java -Xmx50G -jar /group/ag_bsc/vladimir/miniconda2/envs/mapping/share/picard-2.18.29-0/picard.jar MarkDuplicates I=$NAME.bam M=../DEDUPBAMS/$NAME.metrics.txt REMOVE_DUPLICATES=true O=../DEDUPBAMS/$NAME.dedup.bam; date; done`

#Calling variants in total population:

#Adding the sample/group tag

`cd ../DEDUPBAMS/`

`for FILE in *.bam; do samtools index $FILE; done`

`for FILE in *.bam; do NAME=$(basename $FILE .dedup.bam); bamaddrg -b $FILE -s $NAME > ../FREEBAYES/$NAME.RG.bam; done`

#Put all RG.bam files names into allbamlist.txt and call the variants:

`cd ../FREEBAYES/`

`for FILE in *.bam; do samtools index $FILE; done`

`ls *.RG.bam > allbamlist.txt`

`freebayes-parallel <(fasta_generate_regions.py /buffer/ag_bsc/AnjasMacaques/genome/Macaca_mulatta.Mmul_10.dna.toplevel.fa.fai 100000) 20 -f /buffer/ag_bsc/AnjasMacaques/genome/Macaca_mulatta.Mmul_10.dna.toplevel.fa -L allbamlist.txt > ../TOTALPOP/totalpop.vcf`

#Filter for high quality calls (QUAL>30, prob. of polymorphism>0.999, less than 1 in 1000 calls is false positive) and only SNPs

`cd ../TOTALPOP/`

`bcftools view -i 'QUAL>30' -i 'TYPE~"snp"' -O z -o higherquality_snps_secondbatch.vcf.gz --threads 25 totalpop.vcf`


#Calling/phasing genotypes

#Beagle has problem when there is only one variant per scaffold/chromosome, so filtering them and not thoroughy called variants out

`zgrep -v ".:.:.:.:.:.:.:." higherquality_containing_snps.vcf.gz | grep -v "^#" | cut -f1 | uniq -c | awk '{OFS="\t"; print $1,$2}' > number_of_variants_per_chr.txt`

`awk '{if ($1=="1") print $2}' number_of_variants_per_chr.txt > chrs_with_only_one_variant.txt`

`zgrep -v ".:.:.:.:.:.:.:." higherquality_containing_snps.vcf.gz | grep -v -f chrs_with_only_one_variant.txt - > highqual_snps_more_than_1_per_chr.vcf`

`java -Xmx28g -jar /PATH/TO/BEAGLE/beagle.jar gt=highqual_snps_more_than_1_per_chr.vcf out=../PHASING/2ndbatch_phased nthreads=48`

#Conforming the study (low coverage) VCF to reference VCF panel

`cd ../PHASING`

`zgrep -v "^#" 2ndbatch_phased.vcf.gz | cut -f1 | uniq > macaque_chromosomes.txt`

`while IFS= read -r CHROMOSOME; do java -Xmx28g -jar /PATH/TO/CONFORM-GT/conform-gt.24May16.cee.jar gt=2ndbatch_phased.vcf.gz ref=/PATH/TO/REFERENCEPANEL/totalpop_phased_snps_nozeroAF.vcf.gz chrom=$CHROMOSOME out=$CHROMOSOME match=POS; done < macaque_chromosomes.txt`

`awk '{print $0".vcf.gz"}' macaque_chromosomes.txt > ordered_list_of_vcf_files.txt`

`bcftools concat --naive -f ordered_list_of_vcf_files.txt -O z -o 2nd_batch_conformed_variants.vcf.gz`

`java -Xmx28g -jar /PATH/TO/BEAGLE/beagle.jar gt=2nd_batch_conformed_variants.vcf.gz ref=/PATH/TO/REFERENCEPANEL/totalpop_phased_snps_nozeroAF.vcf.gz impute=true window=100 out=2ndbatch_imputed nthreads=48`

#For Noah's pannel

`bcftools view -r 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,X,Y -O z -o 4_1.22chr.preimp.vcf.gz --threads 20 4_1.preimp.vcf.gz`

`zcat 4_1.22chr.preimp.vcf.gz | perl -pe "s/\s\.:/\t.\/.:/g" | bgzip -c > 4_1.22chr.final.vcf.gz`

`while IFS= read -r CHROMOSOME; do java -Xmx28g -jar /group/ag_bsc/bsc_intern/Projects/2020_Widdig_Makaken/SECOND_BATCH/PHASING/beagle/conform-gt.24May16.cee.jar gt=4_1.22chr.final.vcf.gz ref=../cayo.refpanel.vcf.gz chrom=$CHROMOSOME out=$CHROMOSOME match=POS; done < ../5_1_chromosomes.txt`


#IBD calculations

#plink's pi_hat

`cd ../IBD/`

`plink2 --vcf ../TOTALPOP/both_batches.vcf.gz --out both_batches --allow-extra-chr`

`plink2 --bfile both_batches --recode tab --out both_plink --allow-extra-chr`

`plink2 --file both_158 --genome gz full --out both_158_plinkIBDresults --allow-extra-chr`

#ANGSD NgsRelate all calculations

`./ngsRelate -h ../TOTALPOP/both_batches_158chr.vcf.gz -O bothbatches_vcf_ngsrelate.res -c 1 -T GT -p 20`

#GERMLINE2 IBD segments

`plink2 --vcf both_batches_158chr.vcf.gz --make-bed --biallelic-only strict --allow-extra-chr --out totalpop_biallelic`

`cut -f1 totalpop_biallelic.bim | sort -u > list_of_chromosomes.txt`

`while IFS= read -r CHRNAME; do
        plink2 --bfile totalpop_biallelic --chr $CHRNAME --recode 12 --out tmp --allow-extra-chr;
        germline -input tmp.ped tmp.map -output tmp_germline -w_extend -bits 50;
        germline -input tmp.ped tmp.map -output tmp_germline.roh -homoz-only -w_extend -bits 50;
        cat tmp_germline.log >> totalpop12.germline.log;
        cat tmp_germline.match >> totalpop12.germline.match;
        cat tmp_germline.roh.log >> totalpop12.germline.ROH.log;
        cat tmp_germline.roh.match >> totalpop12.germline.ROH.match;
        rm tmp*;
done < list_of_chromosomes.txt`

#IBIS IBD segments

`vcftools --gzvcf 1stbatch_higherquality_snps.vcf.gz --out highQ_snps_DP15 --recode --recode-INFO-all --minDP 15 --maxDP 100`

`bcftools view -Oz -o highQ_snps_DP15.recode.filtered.vcf.gz -g ^miss highQ_snps_DP15.recode.vcf`

`tabix highQ_snps_DP15.recode.filtered.vcf.gz`

`bcftools view -r 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 -Oz -o highQ_snps_DP15.recode.filtered_20chr.vcf.gz highQ_snps_DP15.recode.filtered.vcf.gz`

`plink2 --vcf highQ_snps_DP15.recode.filtered_20chr.vcf.gz --make-bed --out DP15.filtered`

`$IBISPATH/ibis -bfile DP15.filtered -noFamID -o DP15.filtered -printCoef -a 0.0`
